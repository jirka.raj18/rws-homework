﻿using CommandLine;

namespace RwsDocumentProcessor.Processors
{
    public class Options
    {
        [Option('i', "input", Required = true, HelpText = "Input file.")]
        public string InputFilename { get; set; }

        [Option('o', "output", Required = true, HelpText = "Output file.")]
        public string OutputFilename { get; set; }
    }
}
