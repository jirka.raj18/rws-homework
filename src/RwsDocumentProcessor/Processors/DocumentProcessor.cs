﻿using CommandLine;
using RwsDocumentProcessor.Processors.Exceptions;
using RwsDocumentProcessor.Processors.Interfaces;
using RwsDocumentProcessor.Processors.Storages;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

#nullable enable

namespace RwsDocumentProcessor.Processors
{
    public class DocumentProcessor
    {
        private readonly IList<Type> _storageStreams;
        private readonly IList<Type> _formatConvertors;

        public DocumentProcessor()
        {
            _formatConvertors = GetAllTypes<FormatConvertor>();
            _storageStreams = GetAllTypes<StorageStream>();
        }

        public void ProcessCmdLineArgs(string[] args)
        {
            Parser.Default.ParseArguments<Options>(args)
                .WithNotParsed(HandleParseError)
                .WithParsed(Run);
        }

        public Type? GetStorageStreamType(string path, OperationMode mode)
        {
            var result = _storageStreams.FirstOrDefault(f => IsSupported(f, nameof(StorageStream.IsSupported), new object[] { path, mode }));
            return result;
        }

        public Type? GetFormatConvertorType(string path)
        {
            var fileExt = Path.GetExtension(path);
            var result = _formatConvertors.FirstOrDefault(f => IsSupported(f, nameof(FormatConvertor.IsSupportedFileExt), new[] { fileExt }));
            return result;
        }

        private void Run(Options opts)
        {
            var inputFile = GetFileConvertor(opts.InputFilename, OperationMode.Read);
            var outputFile = GetFileConvertor(opts.OutputFilename, OperationMode.Write);
            inputFile.ConvertTo(outputFile);
        }

        private void HandleParseError(IEnumerable<Error> errs)
        {
            // error messages is already displayed by CommandLineParser
            throw new DocumentProcessorException(string.Empty, -2);
        }

        private FormatConvertor GetFileConvertor(string path, OperationMode mode)
        {
            var storageType = GetStorageStreamType(path, mode);
            if (storageType is null)
                throw new DocumentProcessorException($"Unsupported file path '{path}'.", -3);

            StorageStream? storageStream = Activator.CreateInstance(storageType, new[] { path }) as StorageStream;
            if (storageStream is null)
                throw new DocumentProcessorException($"Can not create new instance of class '{storageType}'.", -3);

            if (mode == OperationMode.Read && storageStream.Exists(path) == false)
                throw new DocumentProcessorException($"File '{path}' not exists.", -4);

            // get format processor base on file extension
            var convertorType = GetFormatConvertorType(path);
            if (convertorType is null)
                throw new DocumentProcessorException($"Unsupported file format '{path}'.", -5);

            // create format convertor for file
            FormatConvertor? result = Activator.CreateInstance(convertorType, new[] { storageStream }) as FormatConvertor;
            if (result is null)
                throw new DocumentProcessorException($"Can not create new instance of class '{convertorType}'.", -3);

            return result;
        }

        private static bool IsSupported(Type type, string methodName, object[] parameters)
        {
            var obj = Activator.CreateInstance(type);
            var method = type.GetMethod(methodName);
            if (method is null)
                return false;
            var methodResult = (bool?)method.Invoke(obj, parameters);
            if (!methodResult.HasValue)
                return false;
            return methodResult.Value;
        }

        private static IList<Type> GetAllTypes<T>()
        {
            var result = AppDomain.CurrentDomain
                .GetAssemblies()
                .SelectMany(x => x.GetTypes())
                .Where(x => typeof(T).IsAssignableFrom(x) && !x.IsInterface && !x.IsAbstract)
                .ToList();
            return result;
        }
    }
}
