﻿namespace RwsDocumentProcessor.Processors.Storages
{
    public enum OperationMode
    {
        Read,
        Write
    }
}
