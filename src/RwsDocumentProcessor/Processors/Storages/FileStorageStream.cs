﻿using RwsDocumentProcessor.Processors.Interfaces;
using System;
using System.IO;

namespace RwsDocumentProcessor.Processors.Storages
{
    public class FileStorageStream : StorageStream
    {
        public override string StoragePath => Path.GetFullPath(base.StoragePath);

        public FileStorageStream() : base() { }
        public FileStorageStream(string path) : base(path) { }

        public override Stream CreateReader()
        {
            var result = File.OpenRead(StoragePath);
            return result;
        }

        public override StreamReader CreateTextReader()
        {
            var result = File.OpenText(StoragePath);
            return result;
        }

        public override Stream CreateWriter()
        {
            var result = File.Create(StoragePath);
            return result;
        }

        public override StreamWriter CreateTextWriter()
        {
            var result = File.CreateText(StoragePath);
            return result;
        }

        public override bool Exists(string path)
        {
            var result = File.Exists(path);
            return result;
        }

        public override bool IsSupported(string path, OperationMode mode)
        {
            var result = IsSupportedPath(path) && IsSupportedMode(mode);
            return result;
        }

        public override bool IsSupportedPath(string path)
        {
            for (var i = 0; i < 2; i++)
            {
                if (Uri.TryCreate(path, UriKind.Absolute, out var uri))
                {
                    return uri.IsFile;
                }
                path = Path.GetFullPath(path);
            }
            return false;
        }

        public override bool IsSupportedMode(OperationMode mode)
        {
            var result = (mode == OperationMode.Read) || (mode == OperationMode.Write);
            return result;
        }
    }
}
