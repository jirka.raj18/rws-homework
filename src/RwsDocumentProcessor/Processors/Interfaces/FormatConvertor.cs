﻿using System;

namespace RwsDocumentProcessor.Processors.Interfaces
{
    public abstract class FormatConvertor
    {
        private readonly StorageStream _storageStream;
        protected StorageStream StorageStream => _storageStream;
        public string StoragePath => _storageStream.StoragePath;

        public FormatConvertor() { }
        public FormatConvertor(StorageStream storageStream)
        {
            _storageStream = storageStream;
        }

        public abstract bool IsSupportedFileExt(string fileExt);

        public abstract void Write(Document doc);
        public abstract Document Read();

        public void ConvertTo(FormatConvertor output)
        {
            var doc = Read();
            output.Write(doc);
            Console.WriteLine($"'{StoragePath}' converted to '{output.StoragePath}'");
        }
    }
}
