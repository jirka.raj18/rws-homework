﻿using RwsDocumentProcessor.Processors.Storages;
using System.IO;

namespace RwsDocumentProcessor.Processors.Interfaces
{
    public abstract class StorageStream
    {
        private readonly string _path;
        public virtual string StoragePath => _path;

        public StorageStream() { }
        public StorageStream(string path)
        {
            _path = path;
        }

        public abstract Stream CreateReader();
        public abstract StreamReader CreateTextReader();
        public abstract Stream CreateWriter();
        public abstract StreamWriter CreateTextWriter();

        public abstract bool IsSupported(string path, OperationMode mode);
        public abstract bool IsSupportedMode(OperationMode mode);
        public abstract bool IsSupportedPath(string path);

        public abstract bool Exists(string path);
    }
}
