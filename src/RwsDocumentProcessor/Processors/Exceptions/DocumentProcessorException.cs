﻿using System;

namespace RwsDocumentProcessor.Processors.Exceptions
{
    public class DocumentProcessorException : Exception
    {
        public int ErrorCode { get; private set; }

        public DocumentProcessorException(string message, int errorCode)
            : base(message)
        {
            ErrorCode = errorCode;
        }
    }
}
