﻿using RwsDocumentProcessor.Processors.Exceptions;
using RwsDocumentProcessor.Processors.Interfaces;
using System.Xml.Linq;

namespace RwsDocumentProcessor.Processors.Formats
{
    public class XmlFormatConvertor : FormatConvertor
    {
        public XmlFormatConvertor() : base() { }

        public XmlFormatConvertor(StorageStream storage)
            : base(storage)
        {
        }

        public override Document Read()
        {
            using var reader = StorageStream.CreateReader();
            var xdoc = XDocument.Load(reader);

            if (xdoc.Root.Element("title") is null)
                throw new DocumentProcessorException($"[XML] Missing element 'title'", -101);
            if (xdoc.Root.Element("text") is null)
                throw new DocumentProcessorException($"[XML] Missing element 'text'", -102);

            var result = new Document
            {
                Title = xdoc.Root.Element("title").Value,
                Text = xdoc.Root.Element("text").Value
            };
            return result;
        }

        public override bool IsSupportedFileExt(string fileExt)
        {
            var result = fileExt.ToLower() == ".xml";
            return result;
        }

        public override void Write(Document doc)
        {
            var xdoc = new XDocument(
                new XElement("root",
                    new XElement("title", doc.Title),
                    new XElement("text", doc.Text)
                    )
                );

            using var writer = StorageStream.CreateWriter();
            xdoc.Save(writer);
        }
    }
}
