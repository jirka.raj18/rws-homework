﻿using Newtonsoft.Json;
using RwsDocumentProcessor.Processors.Exceptions;
using RwsDocumentProcessor.Processors.Interfaces;

namespace RwsDocumentProcessor.Processors.Formats
{
    public class JsonFormatConvertor : FormatConvertor
    {
        public JsonFormatConvertor() : base() { }

        public JsonFormatConvertor(StorageStream storage)
            : base(storage)
        {
        }

        public override Document Read()
        {
            var serializer = new JsonSerializer();
            using var reader = StorageStream.CreateTextReader();
            var result = (Document)serializer.Deserialize(reader, typeof(Document));

            if (string.IsNullOrWhiteSpace(result.Title))
                throw new DocumentProcessorException($"[JSON] Missing value for 'title'", -201);
            if (string.IsNullOrWhiteSpace(result.Text))
                throw new DocumentProcessorException($"[JSON] Missing value for 'text'", -202);

            return result;
        }

        public override bool IsSupportedFileExt(string fileExt)
        {
            var result = fileExt.ToLower() == ".json";
            return result;
        }

        public override void Write(Document doc)
        {
            using var writer = StorageStream.CreateTextWriter();
            var serializer = new JsonSerializer();
            serializer.Serialize(writer, doc);
        }
    }
}
