﻿using RwsDocumentProcessor.Processors;
using RwsDocumentProcessor.Processors.Exceptions;
using System;

namespace RwsDocumentProcessor
{
    class Program
    {
        static int Main(string[] args)
        {
            try
            {
                var processor = new DocumentProcessor();
                processor.ProcessCmdLineArgs(args);
                return 0;
            }
            catch (DocumentProcessorException ex)
            {
                if (!string.IsNullOrWhiteSpace(ex.Message))
                    Console.WriteLine(ex.Message);
                return ex.ErrorCode;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return -1;
            }
        }
    }
}
