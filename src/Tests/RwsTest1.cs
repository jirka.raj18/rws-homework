using RwsDocumentProcessor.Processors;
using RwsDocumentProcessor.Processors.Formats;
using RwsDocumentProcessor.Processors.Storages;
using System;
using Xunit;

#nullable enable

namespace Tests
{
    public class RwsTest1
    {
        [Theory]
        [InlineData("c:\\test.json", OperationMode.Read, typeof(FileStorageStream))]
        [InlineData("c:\\test.json", OperationMode.Write, typeof(FileStorageStream))]
        [InlineData("\\..\\test.json", OperationMode.Write, typeof(FileStorageStream))]
        [InlineData("http://test.json", OperationMode.Read, null)]
        public void StorageTest(string path, OperationMode mode, Type? expectedType)
        {
            // Arrange
            var processor = new DocumentProcessor();

            // Act
            var type = processor.GetStorageStreamType(path, mode);

            // Assert
            Assert.Equal(expectedType, type);
        }

        [Theory]
        [InlineData("c:\\test.xml", typeof(XmlFormatConvertor))]
        [InlineData("c:\\test.json", typeof(JsonFormatConvertor))]
        [InlineData("c:\\test.yaml", null)]
        public void ConvertorTest(string path, Type? expectedType)
        {
            // Arrange
            var processor = new DocumentProcessor();

            // Act
            var type = processor.GetFormatConvertorType(path);

            // Assert
            Assert.Equal(expectedType, type);
        }
    }
}
