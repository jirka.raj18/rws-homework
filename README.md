# RWS Homework: Found issues and their explanation (see TODO)

```csharp
using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Linq;
using Newtonsoft.Json;

namespace Moravia.Homework
{
    public class Document
    {
        public string Title { get; set; }
        public string Text { get; set; }
    }

    class Program
    {
        static void Main(string[] args)
        {
            // TODO Issue: it is not exactly issue but better read these parameters from args
            var sourceFileName = Path.Combine(Environment.CurrentDirectory, "..\\..\\..\\Source Files\\Document1.xml");
            var targetFileName = Path.Combine(Environment.CurrentDirectory, "..\\..\\..\\Target Files\\Document1.json");

            try
            {
                FileStream sourceStream = File.Open(sourceFileName, FileMode.Open);
                var reader = new StreamReader(sourceStream);
                string input = reader.ReadToEnd();
            }
            catch (Exception ex)
            {
                // TODO Issue: do not create new exception here because we are at top of application so we should catch all exceptions and display proper message to user
                throw new Exception(ex.Message);
            }

            // TODO Issue: variable "input" is defined and accessible only within try-catch block above
            var xdoc = XDocument.Parse(input);
            var doc = new Document
            {
                // TODO Issue: we do not know if source XML file contains elements below -> cover with try-catch block or check if element exists or not
                // I assume that both elements are MANDATORY
                Title = xdoc.Root.Element("title").Value,
                Text = xdoc.Root.Element("text").Value
            };

            var serializedDoc = JsonConvert.SerializeObject(doc);

            // TODO Issue: we are saving data to file so there could raise exception -> cover these code block below with try-catch
            var targetStream = File.Open(targetFileName, FileMode.Create, FileAccess.Write);
            var sw = new StreamWriter(targetStream);
            sw.Write(serializedDoc);
        }
    }
}
```